import src.course_management
import src.work_with_plan
import src.calendar.сalendar
import src.work_with_my_courses
from config import bot
from src.common_data import start_markup
from src.queries import set_user


@bot.callback_query_handler(func=lambda call: call.data == 'start')
def callback_start(call):
    bot.send_message(chat_id=call.message.chat.id,
                     text="Что вы хотите сделать?",
                     reply_markup=start_markup)


@bot.message_handler(commands=['start'])
def start(message):
    set_user(message.from_user.id, message.from_user.username)
    bot.send_message(chat_id=message.chat.id,
                     text="Привет, Что вы хотите сделать?",
                     reply_markup=start_markup)


try:
    bot.polling()
except:
    bot.polling()
