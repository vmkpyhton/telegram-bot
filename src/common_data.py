from telebot import types

from config import bot

current_action = {}
course_data = {}
workplan_data = {}
homework_data = {}
current_shown_dates = {}
message_to_user = {}

start_markup = types.InlineKeyboardMarkup(row_width=1)
start_markup.add(types.InlineKeyboardButton(
    text='Управление вашими курсами', callback_data='course_management'
), types.InlineKeyboardButton(
    text='Курсы на котрые вы подписанны', callback_data='my_course'
))


class ExceptionMessage(Exception):
    def __init__(self):
        pass

    @staticmethod
    def show_started(message):
        bot.send_message(chat_id=message.chat.id,
                         text="Что то пошло не так, Что вы хотите сделать?",
                         reply_markup=start_markup
                         )
