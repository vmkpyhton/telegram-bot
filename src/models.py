from psycopg2 import extras

from config import conn


class Homework:
    def __init__(self, id_workplan='', date='', max_mark='', information='', id='', id_student='', mark=''):
        self.information = information
        self.deadline = date
        self.id_workplan = id_workplan
        self.max_mark = max_mark
        self.id = id
        self.id_student = id_student
        self.mark = mark

    def __str__(self):
        return 'Информация по домашней работе\n{0}\nМаксимальная оценка - {1}\nКрайний срок {2}'.format(
            self.information,
            self.max_mark, self.deadline.strftime("%d.%m.%Y"))

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return self.id == other.id

    def add_homework(self):
        cursor = conn.cursor()
        cursor.execute(
            "insert into homework (id_workplan, date_end, max_mark, information) "
            "values({0},'{1}',{2},'{3}') RETURNING id".format(self.id_workplan, self.deadline.strftime("%Y-%m-%d"),
                                                              self.max_mark,
                                                              self.information))
        self.id = cursor.fetchall()[0][0]
        cursor.close()

    def update_homework(self):
        cursor = conn.cursor()
        cursor.execute(
            "update homework "
            "set max_mark = '{0}', "
            "information = '{1}', "
            "date_end = '{2}' "
            "where id = {3}".format(self.max_mark, self.information, self.deadline, self.id))
        cursor.close()

    @staticmethod
    def fetch_homework(id_workplan):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select * from homework where id_workplan={id_workplan}")
        homeworks = [Homework(id_workplan, homework['date_end'], homework['max_mark'], homework['information'],
                              homework['id']) for homework in cursor.fetchall()]
        cursor.close()
        return homeworks

    def get_homework(self, homework_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select * from homework where id={homework_id}")
        data = cursor.fetchall()[0]
        self.id_workplan, self.id = data['id_workplan'], data['id']
        self.information = data['information']
        self.deadline = data['date_end']
        self.max_mark = data['max_mark']
        cursor.close()
        return self

    @staticmethod
    def get_status_homework(homework_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select * from uspev where id_homework={homework_id}")
        data = cursor.fetchall()
        cursor.close()
        return data

    @staticmethod
    def get_all_homework_by_course(course_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select h.id,h.id_workplan,h.date_end,h.max_mark,h.information from homework h "
                       f"join workplan w2 ON h.id_workplan = w2.id "
                       f"JOIN kurs k ON w2.id_kurs = k.id_kurs WHERE k.id_kurs = {course_id}")
        for course in cursor.fetchall():
            yield Homework(course['id_workplan'], course['date_end'], course['max_mark'], course['information'],
                           course['id'])

    @staticmethod
    def pass_homework(id, user_id, work):
        cursor = conn.cursor()
        cursor.execute(
            f'insert into uspev(id_student,id_homework,work) values({user_id},{id},\'{work}\') on conflict(id,id_student) do '
            f'update set work = excluded.work')
        cursor.close()

    @staticmethod
    def add_mark(id, mark):
        cursor = conn.cursor()
        cursor.execute(
            f'update uspev set mark={mark} where id={id}')
        cursor.close()

    @staticmethod
    def add_comment(id, comment):
        cursor = conn.cursor()
        cursor.execute(
            f'update uspev set comment=\'{comment}\' where id={id}')
        cursor.close()

    @staticmethod
    def get_all_homework_by_course_for_student(course, student):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        homeworks = Homework.get_all_homework_by_course(course)
        cursor.execute(
            f'select id_student, mark,id_homework from uspev where id_student={student}')
        students_homework = {}
        for homework in cursor.fetchall():
            students_homework[homework['id_homework']] = Homework(id=homework['id_homework'],
                                                                  id_student=homework['id_student'],
                                                                  mark=homework['mark'])
        data = []
        for homework in homeworks:
            home = students_homework.get(homework.id, homework)
            home.information = homework.information
            home.mark = homework.mark if homework.mark is not None else ''
            data.append(home)
        cursor.close()
        return data


class WorkPlan:
    def __init__(self, id_course, date='', theme='', information='', id='', fetch_homework=False):
        self.information = information
        self.date = date
        self.theme = theme
        self.id_course = id_course
        self.homework = []
        self.id = id
        if fetch_homework:
            Homework.fetch_homework(self.id)

    def __str__(self):
        return 'Тема- {0}\nИнформация - {1}\n' \
               'Дата - {2}\n{3}'.format(self.theme, self.information,
                                        self.date.strftime("%d.%m.%Y"),
                                        '\n'.join(['/home{0} '.format(homework.id) + str(homework)
                                                   for homework in self.homework]))

    def get_plan(self, plan_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select * from workplan where id={plan_id}")
        data = cursor.fetchall()[0]
        self.theme, self.id = data['theme'], data['id']
        self.information = data['information']
        self.date = data['date_plan']
        self.id_course = data['id_kurs']
        self.homework = Homework.fetch_homework(self.id)
        cursor.close()
        return self

    def add_plan(self):
        cursor = conn.cursor()
        cursor.execute(
            "insert into workplan (id_kurs, theme, information, date_plan) "
            "values({0},'{1}','{2}','{3}') RETURNING id".format(self.id_course, self.theme, self.information,
                                                                self.date.strftime("%Y-%m-%d")))
        self.id = cursor.fetchall()[0][0]
        cursor.close()

    def update_plan(self):
        cursor = conn.cursor()
        cursor.execute(
            "update workplan "
            "set theme = '{0}', "
            "information = '{1}', "
            "date_plan = '{2}' "
            "where id = {3}".format(self.theme, self.information, self.date, self.id))
        cursor.close()

    def get_recived_homework(self):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select uspev.id,uspev.work, u.username FROM uspev "
                       f"JOIN homework h2 ON uspev.id_homework = h2.id "
                       f"join users u ON uspev.id_student = u.id "
                       f"WHERE h2.id_workplan={self.id} and (uspev.comment IS not NULL or uspev.mark is null)")
        data = cursor.fetchall()
        cursor.close()
        return data

    @staticmethod
    def fetch_workplans(id_course):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select * from workplan where id_kurs={id_course}")
        workplans = [WorkPlan(id_course, workplan['date_plan'], workplan['theme'], workplan['information'],
                              workplan['id'], True) for workplan in cursor.fetchall()]
        cursor.close()
        return workplans


class Course:
    def __init__(self, name_course, teacher, short_info='', date_start='', date_finish='', info='', id=''):
        self.date_start = date_start
        self.date_finish = date_finish
        self.info = info
        self.id = id
        self.name_course = name_course
        self.short_info = short_info
        self.teacher = teacher

    def __str__(self):
        return 'Название курса - {0}\nКраткое описание - {1}\n' \
               'Полное описание - {2}\nс {3} по {4}\n'.format(self.name_course, self.short_info,
                                                              self.info, self.date_start.strftime("%d.%m.%Y"),
                                                              self.date_finish.strftime("%d.%m.%Y"))

    def add_course(self):
        cursor = conn.cursor()
        cursor.execute(
            "insert into kurs (name_kurs,short_info,info,course_period,id_teacher) "
            "values('{0}','{1}','{2}','[{3},{4}]', {5}) RETURNING id_kurs".format(self.name_course, self.short_info,
                                                                                  self.info,
                                                                                  self.date_start.strftime("%Y-%m-%d"),
                                                                                  self.date_finish.strftime("%Y-%m-%d"),
                                                                                  self.teacher))
        self.id = cursor.fetchall()[0][0]
        cursor.close()

    def update_course(self):
        cursor = conn.cursor()
        cursor.execute(
            "update kurs "
            "set name_kurs = '{0}', "
            "short_info = '{1}', "
            "info = '{2}', "
            "course_period = '[{3},{4}]', "
            "id_teacher = {5} "
            "where id_kurs = {6}".format(self.name_course, self.short_info, self.info,
                                         self.date_start.strftime("%Y-%m-%d"),
                                         self.date_finish.strftime("%Y-%m-%d"),
                                         self.teacher,
                                         self.id))
        cursor.close()

    def get_course_info(self, course_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select * from kurs where id_kurs={course_id}")
        data = cursor.fetchall()[0]
        self.name_course, self.teacher = data['name_kurs'], data['id_teacher']
        self.short_info = data['short_info']
        self.info = data['info']
        self.date_start = data['course_period'].lower
        self.date_finish = data['course_period'].upper
        self.id = data['id_kurs']
        cursor.close()
        return self

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return self.id == other.id

    @staticmethod
    def get_course_teacher_by_homework(homework_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(f"select id_teacher,kurs.name_kurs from kurs "
                       f"JOIN workplan w2 ON kurs.id_kurs = w2.id_kurs "
                       f"JOIN homework h2 ON w2.id = h2.id_workplan "
                       f"WHERE h2.id =  {homework_id}")
        data = cursor.fetchall()[0]
        cursor.close()
        return data

    # Просмотр курсов. на которые записан студент
    @staticmethod
    def get_course_by_user(user_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(
            "select kurs.id_kurs as id,kurs.name_kurs as name_kurs, "
            "kurs.short_info as short_info, kurs.info as info, "
            "kurs.course_period as course_period, kurs.id_teacher as id_teacher "
            "from kurs_student,kurs where kurs_student.id_kurs=kurs.id_kurs and kurs_student.id_student = %s" % user_id)
        for course in cursor.fetchall():
            yield Course(course['name_kurs'], course['id_teacher'], course['short_info'], course['course_period'].lower,
                         course['course_period'].upper, course['info'], course['id'])
        cursor.close()

    @staticmethod
    def fetch_courses():
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute("SELECT * FROM kurs")
        for course in cursor.fetchall():
            yield Course(course['name_kurs'], course['id_teacher'], course['short_info'], course['course_period'].lower,
                         course['course_period'].upper, course['info'], course['id_kurs'])
        cursor.close()

    @staticmethod
    def fetch_course_without_user_courses(user_id):
        return set(Course.fetch_courses()).difference(set(Course.get_course_by_user(user_id)))

    @staticmethod
    def delete_course(course_id):
        cursor = conn.cursor()
        cursor.execute("DELETE FROM kurs WHERE kurs.id_kurs=" + str(course_id))
        cursor.close()

    @staticmethod
    def get_students(course_id):
        cursor = conn.cursor(cursor_factory=extras.DictCursor)
        cursor.execute(
            "SELECT AVG(coalesce(uspev.mark,0)) AS avg_mark, "
            "uspev.id_student AS id_student,u.username AS username FROM uspev "
            "JOIN users u ON uspev.id_student = u.id "
            "join kurs_student k_s on uspev.id_student = k_s.id_student and k_s.id_kurs = {0} "
            "group BY uspev.id_student,u.username ".format(course_id))
        data = cursor.fetchall()
        cursor.close()
        return data

    @staticmethod
    def leave_course(course_id, user_id):
        cursor = conn.cursor()
        cursor.execute(f"delete from kurs_student WHERE id_student={user_id} and id_kurs={course_id}")
        cursor.execute(f"delete from uspev WHERE id_student={user_id}")
        cursor.close()
