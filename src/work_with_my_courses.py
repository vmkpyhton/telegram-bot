from telebot import types

from config import bot
from src.common_data import current_action, ExceptionMessage
from src.models import Course
from src.queries import add_user_for_course


@bot.callback_query_handler(lambda call: call.data == 'my_course')
def work_with_my_course(callback):
    try:
        message = callback.message
    except Exception as e:
        message = callback
    chat_id = message.chat.id
    courses = Course.get_course_by_user(chat_id)
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton(
        text='Вернутсья в начало', callback_data='start'
    ), types.InlineKeyboardButton(
        text='Записаться на курс', callback_data='register_on_course'
    ))
    current_action[chat_id] = 'my_course'
    bot.send_message(
        chat_id=chat_id,
        text='\n'.join(['Курсы на которые вы подписанны:'] + [
            '/course{0} '.format(course.id) +
            str(course) for course in courses
        ]),
        reply_markup=markup
    )


@bot.callback_query_handler(lambda call: call.data[:len('leave_course-')] == 'leave_course-')
def leave_course(call):
    id_course = call.data[len('leave_course-'):]
    Course.leave_course(id_course, call.message.chat.id)
    work_with_my_course(call.message)

available_courses = {}


def add_courses_markup(chat_id, change):
    n = len(available_courses[chat_id]['courses']) - 1
    available_courses[chat_id]['left'] = max(0, min(max(0, n - 3), available_courses[chat_id]['left'] + change))
    available_courses[chat_id]['right'] = max(3, min(n, available_courses[chat_id]['right'] + change))
    left = available_courses[chat_id]['left']
    right = available_courses[chat_id]['right']
    courses = available_courses[chat_id]['courses'][left:right]
    markup = types.InlineKeyboardMarkup(row_width=5)
    markup.add(
        types.InlineKeyboardButton('<', callback_data='last_courses')
    )
    for course in courses:
        markup.add(
            types.InlineKeyboardButton(course.name_course, callback_data='register_course-{0}'.format(course.id))
        )
    markup.add(
        types.InlineKeyboardButton('>', callback_data='right_courses')
    )
    return (courses, markup)


@bot.callback_query_handler(func=lambda call: call.data == 'register_on_course')
def register_on_course(call):

    chat_id = call.message.chat.id
    available_courses[chat_id] = {'courses': list(Course.fetch_course_without_user_courses(chat_id)), 'left': 0,
                                  'right': 3}
    courses, markup = add_courses_markup(chat_id, 0)
    bot.send_message(chat_id, '\n'.join(['Выберите курс на который хотите записаться']
                                        + [str(course) for course in courses]), reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == 'last_courses')
def next_course(call):
    chat_id = call.message.chat.id
    courses, markup = add_courses_markup(chat_id, -1)
    try:
        bot.edit_message_text('\n'.join(['Выберите курс на который хотите записаться']
                                        + [str(course) for course in courses]), call.from_user.id,
                              call.message.message_id,
                              reply_markup=markup)
        bot.answer_callback_query(call.id, text="")
    except:
        print('start')


@bot.callback_query_handler(func=lambda call: call.data == 'right_courses')
def next_course(call):
    chat_id = call.message.chat.id
    courses, markup = add_courses_markup(chat_id, 1)
    try:
        bot.edit_message_text('\n'.join(['Выберите курс на который хотите записаться']
                                        + [str(course) for course in courses]), call.from_user.id,
                              call.message.message_id,
                              reply_markup=markup)
        bot.answer_callback_query(call.id, text="")
    except:
        print('end')


@bot.callback_query_handler(func=lambda call: call.data[0:16] == 'register_course-')
def register_course(call):
    try:
        id_kurs = call.data[16:]
        add_user_for_course(int(id_kurs), call.message.chat.id)
        work_with_my_course(call.message)
        print(id_kurs)
    except Exception as e:
        print(e)
        ExceptionMessage.show_started(call.message)
