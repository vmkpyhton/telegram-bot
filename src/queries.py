import psycopg2.extras as extras
from datetime import datetime

from config import conn
import json
from telebot import types
from config import bot


def get_teacher_courses(teacher_id):
    cursor = conn.cursor(cursor_factory=extras.DictCursor)
    cursor.execute('select id_kurs,name_kurs from kurs where id_teacher={0} ORDER BY name_kurs'.format(teacher_id))
    data = cursor.fetchall()
    cursor.close()
    return data


def get_user_by_id(user_id):
    cursor = conn.cursor()
    cursor.execute("select * from users where id = {0}".format(user_id))
    data = cursor.fetchall()
    cursor.close()
    return data


def set_user(user_id, username):
    cursor = conn.cursor()
    cursor.execute(f"insert into users values({user_id},'{username}') ON CONFLICT DO NOTHING")
    cursor.close()


# Запись в бд о том, что ученик записался на определенный курс
# При этом проверяем, записан ли ученик уже на этот курс
def add_user_for_course(id_kurs, user_id):
    cursor = conn.cursor()
    cursor.execute('insert into kurs_student(id_student,id_kurs) VALUES ({0}, {1})'.format(user_id, id_kurs))
    cursor.close()


# При выборе определенного курса ученик может увидеть , какие действия он может сделать.Например,
# Просмотреть описание курса,текущее дз, рейтинг
def function_of_course(msg):
    id_kurs = msg.text
    keyboard = types.InlineKeyboardMarkup()
    keyboard.row(
        types.InlineKeyboardButton('Просмотреть описание курса',
                                   callback_data=json.dumps(
                                       {'action': 'show_description_of_course', 'id_kurs': id_kurs})),
        types.InlineKeyboardButton('Просмотреть текущее домашнее задание',
                                   callback_data=json.dumps({'action': 'show_homework', 'id_kurs': id_kurs})),
        types.InlineKeyboardButton('Просмотреть свой рейтинг',
                                   callback_data=json.dumps({'action': 'show_my_raiting', 'id_kurs': id_kurs})),
    )
    bot.send_message(chat_id=msg.chat.id, text='Что вы хотите сделать?\n',
                     reply_markup=keyboard)


# Просмотр описания курса
def description_of_course(kurs):
    cursor = conn.cursor()
    cursor.execute(
        "select name_kurs,short_info,info from kurs where id_kurs = %s" % kurs)
    data = cursor.fetchall()
    cursor.close()
    return data


# Показываем текущее домашнее задание
def homework_of_course(kurs):
    cursor = conn.cursor()
    cursor.execute(
        '''select h2.id as id,h2.information as info,date_end,max_mark FROM homework as h2 
        join workplan w2 ON h2.id_workplan = w2.id 
        JOIN kurs k ON w2.id_kurs = k.id_kurs where k.id_kurs = {0} and date_end<'{1}' 
        '''.format(kurs, datetime.now().strftime("%Y-%m-%d")))
    data = cursor.fetchall()
    cursor.close()
    return data


# Показываем рейтинг для ученика
def raiting_of_course(user, kurs):
    cursor = conn.cursor()
    cursor.execute(
        f'''select mark from uspev as u join homework as h2 on h2.id = u.id_homework 
        join workplan w2 ON h2.id_workplan = w2.id 
        JOIN kurs k ON w2.id_kurs = k.id_kurs where k.id_kurs = {kurs} and u.id_student={user}''')
    data = cursor.fetchall()
    cursor.close()
    return data
