import re
from telebot import types

from config import bot
from src.course_management import get_calendar
from src.common_data import current_action, workplan_data, homework_data, ExceptionMessage
from src.models import Homework, Course
from src.work_with_plan import work_with_plan


@bot.message_handler(regexp="home([0-9]+)")
def work_with_home(message):
    try:
        id_homework = re.search("home([0-9]+)", message.text, re.IGNORECASE)
        if id_homework:
            id_homework = id_homework.group(1)
            homework_data[message.chat.id] = Homework('', '')
            homework_data[message.chat.id].get_homework(id_homework)
        markup = types.InlineKeyboardMarkup(row_width=1)
        if current_action[message.chat.id] == 'workplan_management' or current_action[
            message.chat.id] == 'edit_homework' or current_action[message.chat.id] == 'edit_workplan':
            markup.add(types.InlineKeyboardButton(
                text='Вернутсья в теме', callback_data='work_with_plan'
            ), types.InlineKeyboardButton(
                text='Изменить максимальную оценку', callback_data='change_max_mark'
            ), types.InlineKeyboardButton(
                text='Изменить срок сдачи', callback_data='change_date_homework'
            ), types.InlineKeyboardButton(
                text='Изменить суть задачи', callback_data='change_home_info'
            ))
            current_action[message.chat.id] = 'edit_homework'
        elif current_action[message.chat.id] == 'my_course':
            markup.add(types.InlineKeyboardButton(
                text='Вернуться в начало', callback_data='start'
            ), types.InlineKeyboardButton(
                text='Сдать ДЗ/Пересдать ДЗ', callback_data='pass_homework-' + id_homework
            ), types.InlineKeyboardButton(
                text='Посмотреть статус по ДЗ', callback_data='status_homework-' + id_homework
            ))
        bot.send_message(chat_id=message.chat.id,
                         text='\n'.join(
                             [
                                 str(homework_data[message.chat.id]),
                                 "Что вы хотите делать с ДЗ"
                             ]
                         ), reply_markup=markup)
    except Exception as e:
        print(e)
        ExceptionMessage.show_started(message)


pass_homework_id = {}


@bot.callback_query_handler(lambda call: call.data[:len('pass_homework-')] == 'pass_homework-')
def click_pass_homework(call):
    id_homework = call.data[len('pass_homework-'):]
    pass_homework_id[call.message.chat.id] = id_homework
    msg = bot.send_message(call.message.chat.id, text="Введите информацию по ДЗ")
    bot.register_next_step_handler(msg, pass_homework)


def pass_homework(message):
    work = message.text
    Homework.pass_homework(pass_homework_id[message.chat.id], message.chat.id, work)
    course = Course.get_course_teacher_by_homework(pass_homework_id[message.chat.id])
    bot.send_message(course['id_teacher'], text=f"По курсу {course['name_kurs']}  пришла новая ДЗ")
    message.text = 'home' + pass_homework_id[message.chat.id]
    work_with_home(message)


@bot.callback_query_handler(lambda call: call.data[:len('status_homework-')] == 'status_homework-')
def status_homework(call):
    id_homework = call.data[len('status_homework-'):]
    status = Homework.get_status_homework(id_homework)

    message = call.message
    if len(status) > 0:
        bot.send_message(call.message.chat.id, text="Вы ничего не прислали")
    else:
        status = status[0]
        if status['mark'] is None and status['comment'] is None:
            bot.send_message(call.message.chat.id, text="Не просмотренно")
        elif status['mark'] is not None:
            bot.send_message(call.message.chat.id, text=f"Оценка - {status['mark']}")
        elif status['comment'] is not None:
            bot.send_message(call.message.chat.id, text=f"Коментарий к работе - {status['comment']}")
        elif status['comment'] is not None and status['mark'] is not None:
            bot.send_message(call.message.chat.id,
                             text=f"Коментарий к работе - {status['comment']}\n Оценка - {status['mark']}")
    message.text = 'home' + id_homework
    work_with_home(message)


@bot.callback_query_handler(func=lambda call: call.data == 'change_home_info')
def change_theme(call):
    msg = bot.reply_to(call.message, """
        Напишите Информацию по ДЗ
        """)
    bot.register_next_step_handler(msg, step_info_homework)


@bot.callback_query_handler(func=lambda call: call.data == 'change_max_mark')
def change_information(call):
    msg = bot.reply_to(call.message, 'Напишите максимальный балл')
    bot.register_next_step_handler(msg, step_max_mark)


@bot.callback_query_handler(func=lambda call: call.data == 'change_date_homework')
def change_date(call):
    bot.reply_to(call.message, 'Выберите крайний срок сдачи')
    get_calendar(call.message)


@bot.callback_query_handler(func=lambda call: call.data == 'add_homework')
def add_homework(call):
    chat_id = call.message.chat.id
    msg = bot.reply_to(call.message, """
        Напишите Информацию по ДЗ
        """)
    current_action[chat_id] = 'add_homework'
    bot.register_next_step_handler(msg, step_info_homework)


def step_info_homework(message):
    try:
        chat_id = message.chat.id
        info = message.text
        if current_action[message.chat.id] != 'edit_homework':
            homework = Homework(workplan_data[chat_id].id, information=info)
            homework_data[chat_id] = homework
            msg = bot.reply_to(message, 'Напишите максимальный балл')
            bot.register_next_step_handler(msg, step_max_mark)
        else:
            workplan_data[chat_id].information = info
            to_accept_homework(message, homework_data[chat_id])

    except Exception as e:
        msg = bot.reply_to(message, 'Ошибочка\nНапишите Информацию по ДЗ')
        bot.register_next_step_handler(msg, step_info_homework)


def step_max_mark(message):
    try:
        chat_id = message.chat.id
        homework_data[chat_id].max_mark = message.text
        if current_action[message.chat.id] != 'edit_homework':
            bot.reply_to(message, 'Выберите крайний срок сдачи')
            get_calendar(message)
        else:
            to_accept_homework(message, homework_data[chat_id])
    except Exception as e:
        msg = bot.reply_to(message, 'Ошибочка\nНапишите максимальный балл')
        bot.register_next_step_handler(msg, step_max_mark)


def to_accept_homework(message, homework):
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton(
        text='Да', callback_data='accept_register_home/Yes'
    ), types.InlineKeyboardButton(
        text='Нет', callback_data='accept_register_home/No'
    ))
    bot.reply_to(message,
                 str(homework) + '\nВсе верно?',
                 reply_markup=markup)


@bot.callback_query_handler(
    func=lambda call: call.data == 'accept_register_home/Yes' or call.data == 'accept_register_home/No')
def accept_register_homework(call):
    chat_id = call.message.chat.id
    if current_action[chat_id] != 'edit_homework':
        if call.data == 'accept_register_home/Yes':
            homework_data[chat_id].add_homework()
            current_action[chat_id] = 'workplan_management'
            call.message.text = 'theme' + str(workplan_data[chat_id].id)
            work_with_plan(call.message)
        else:
            add_homework(call)
    else:
        if call.data == 'accept_register_home/Yes':
            homework_data[chat_id].update_homework()
        current_action[chat_id] = 'workplan_management'
        call.message.text = 'theme' + str(workplan_data[chat_id].id)
        work_with_plan(call.message)
