import re
from telebot import types

from config import bot
from src.course_management import get_calendar
from src.common_data import course_data, current_action, workplan_data, ExceptionMessage
from src.models import WorkPlan, Homework


@bot.callback_query_handler(func=lambda call: call.data == 'work_with_plan')
def work_with_plans(call):
    chat_id = call.message.chat.id
    workplans = WorkPlan.fetch_workplans(course_data[chat_id].id)
    markup = types.InlineKeyboardMarkup(row_width=1)
    markup.add(types.InlineKeyboardButton(
        text='Вернутсья в начало', callback_data='start'
    ), types.InlineKeyboardButton(
        text='Добавить тему', callback_data='add_work_plan'
    ))
    current_action[chat_id] = 'workplan_management'
    bot.send_message(
        chat_id=chat_id,
        text='\n'.join(['Темы курса:'] + [
            '/theme{0} '.format(workplan.id) +
            str(workplan) for workplan in workplans
        ]),
        reply_markup=markup
    )


@bot.message_handler(regexp="theme([0-9]{1,})")
def work_with_plan(message):
    try:
        id_plan = re.search("theme([0-9]*)", message.text, re.IGNORECASE)
        if id_plan:
            id_course = id_plan.group(1)
            workplan_data[message.chat.id] = WorkPlan('', '')
            workplan_data[message.chat.id].get_plan(id_course)

        markup = types.InlineKeyboardMarkup(1)
        markup.add(types.InlineKeyboardButton(
            text='Вернутсья в начало', callback_data='start'
        ), types.InlineKeyboardButton(
            text='Посмотреть присланные Д/З', callback_data='recived_homeworks'
        ), types.InlineKeyboardButton(
            text='Добавить Д/З', callback_data='add_homework'
        ), types.InlineKeyboardButton(
            text='Изменить тему плана', callback_data='change_theme'
        ), types.InlineKeyboardButton(
            text='Изменить информацию по теме', callback_data='change_information'
        ), types.InlineKeyboardButton(
            text='Изменить дату', callback_data='change_date'
        ))
        if current_action[message.chat.id] == 'workplan_management' or current_action[
            message.chat.id] == 'edit_workplan':
            current_action[message.chat.id] = 'edit_workplan'
            bot.send_message(chat_id=message.chat.id,
                             text='\n'.join(
                                 [
                                     str(workplan_data[message.chat.id]),
                                     "Что вы хотите сделать с темой"
                                 ]
                             ), reply_markup=markup)
    except Exception as e:
        print(e)
        ExceptionMessage.show_started(message)


@bot.callback_query_handler(lambda call: call.data == 'recived_homeworks')
def recived_homeworks(call):
    try:
        rec_homeworks = workplan_data[call.message.chat.id].get_recived_homework()
        for rec in rec_homeworks:
            murkup = types.InlineKeyboardMarkup(row_width=1)
            murkup.add(types.InlineKeyboardButton(
                'Поставить оценку(учтите чтоелси поставите оценку без коментария то это будет навсегда)',
                callback_data='mark-' + str(rec['id'])),
                types.InlineKeyboardButton('Прокоментировать', callback_data='сomment-' + str(rec['id']))
            )
            bot.send_message(call.message.chat.id,
                             text=f"Вам пришло сообщение от студента {rec['username']}\nСодержание:\n{rec['work']}",
                             reply_markup=murkup)
    except Exception as e:
        print(e)
        ExceptionMessage.show_started(call.message)


uspev_data = {}


@bot.callback_query_handler(lambda call: call.data[:5] == 'mark-' or call.data[:8] == 'сomment-')
def mark_coment(call):
    if call.data[:5] == 'mark-':
        uspev_data[call.message.chat.id] = call.data[5:]
        msg = bot.send_message(call.message.chat.id, "Оценка?")
        bot.register_next_step_handler(msg, add_mark)
    else:
        uspev_data[call.message.chat.id] = call.data[8:]
        msg = bot.send_message(call.message.chat.id, "Прокоментируйте")
        bot.register_next_step_handler(msg, add_comment)


def add_mark(message):
    uspev_id = uspev_data[message.chat.id]
    mark = message.text
    Homework.add_mark(uspev_id, mark)
    message.text = 'theme' + str(workplan_data[message.chat.id].id)
    work_with_plan(message)


def add_comment(message):
    uspev_id = uspev_data[message.chat.id]
    Homework.add_comment(uspev_id, message.text)
    message.text = 'theme' + str(workplan_data[message.chat.id].id)
    work_with_plan(message)


@bot.callback_query_handler(func=lambda call: call.data == 'change_theme')
def change_theme(call):
    msg = bot.reply_to(call.message, """
        Напишите тему
        """)
    bot.register_next_step_handler(msg, step_theme_workplan)


@bot.callback_query_handler(func=lambda call: call.data == 'change_information')
def change_information(call):
    msg = bot.reply_to(call.message, 'Напишите информацию по теме')
    bot.register_next_step_handler(msg, step_info_workplan)


@bot.callback_query_handler(func=lambda call: call.data == 'change_date')
def change_date(call):
    bot.reply_to(call.message, 'Выберите дату темы')
    get_calendar(call.message)


@bot.callback_query_handler(func=lambda call: call.data == 'add_work_plan')
def add_work_plan(call):
    chat_id = call.message.chat.id
    msg = bot.reply_to(call.message, """
        Напишите тему
        """)
    current_action[chat_id] = 'add_new_theme'
    bot.register_next_step_handler(msg, step_theme_workplan)


def step_theme_workplan(message):
    print(message)
    try:
        chat_id = message.chat.id
        theme = message.text
        if current_action[message.chat.id] != 'edit_workplan':
            workplan = WorkPlan(course_data[chat_id].id, theme=theme)
            workplan_data[chat_id] = workplan
            msg = bot.reply_to(message, 'Напишите информацию по теме')
            bot.register_next_step_handler(msg, step_info_workplan)
        else:
            workplan_data[chat_id].theme = theme
            to_accept_workplan(message, workplan_data[chat_id])

    except Exception as e:
        msg = bot.reply_to(message, 'Ошибочка\nНапишите название темы')
        bot.register_next_step_handler(msg, step_theme_workplan)


def step_info_workplan(message):
    try:
        chat_id = message.chat.id
        workplan_data[chat_id].information = message.text
        if current_action[message.chat.id] != 'edit_workplan':
            bot.reply_to(message, 'Выберите дату темы')
            get_calendar(message)
        else:
            to_accept_workplan(message, workplan_data[chat_id])
    except Exception as e:
        msg = bot.reply_to(message, 'Ошибочка\nНапишите информацию по теме')
        bot.register_next_step_handler(msg, step_info_workplan)


def to_accept_workplan(message, workplan):
    markup = types.InlineKeyboardMarkup(2)
    markup.add(types.InlineKeyboardButton(
        text='Да', callback_data='accept_register/Yes'
    ), types.InlineKeyboardButton(
        text='Нет', callback_data='accept_register/No'
    ))
    bot.reply_to(message,
                 str(workplan) + '\nВсе верно?',
                 reply_markup=markup)


@bot.callback_query_handler(func=lambda call: call.data == 'accept_register/Yes' or call.data == 'accept_register/No')
def accept_register_workplan(call):
    chat_id = call.message.chat.id
    if current_action[chat_id] != 'edit_workplan':
        if call.data == 'accept_register/Yes':
            workplan_data[chat_id].add_plan()
            work_with_plans(call)
        else:
            add_work_plan(call)
    else:
        if call.data == 'accept_register/Yes':
            workplan_data[chat_id].update_plan()
        current_action[chat_id] = 'workplan_management'
        call.message.text = 'theme' + str(workplan_data[chat_id].id)
        work_with_plan(call.message)
