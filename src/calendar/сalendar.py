#!/usr/bin/python3
from config import bot
import datetime
from src.calendar.telegramcalendar import create_calendar
from src import course_management as management, work_with_homework
import src.work_with_plan as work_with_plan
from src.common_data import current_action, course_data, workplan_data, current_shown_dates, homework_data


@bot.callback_query_handler(func=lambda call: call.data[0:13] == 'calendar-day-')
def get_day(call):
    chat_id = call.message.chat.id
    saved_date = current_shown_dates.get(chat_id)
    if saved_date is not None:
        day = call.data[13:]
        date = datetime.datetime(int(saved_date[0]), int(saved_date[1]), int(day), 0, 0, 0)
        if current_action[chat_id] == 'date_start' or current_action[chat_id] == 'edit_course':
            course_data[chat_id].date_start = date
            if current_action[chat_id] == 'date_start':
                current_action[chat_id] = 'date_end'
            else:
                current_action[chat_id] = 'edit_course_2'
            bot.reply_to(call.message, 'Введите дату конца курса')
            management.get_calendar(call.message)
        elif current_action[chat_id] == 'date_end' or current_action[chat_id] == 'edit_course_2':
            course_data[chat_id].date_finish = date
            course = course_data[chat_id]
            if current_action[chat_id] == 'date_end':
                current_action[chat_id] = 'aссept_register_course'
            else:
                current_action[chat_id] = 'edit_course'
            management.to_accept(call.message, course)
        elif current_action[chat_id] == 'add_new_theme' or current_action[chat_id] == 'edit_workplan':
            if current_action[chat_id] == 'add_new_theme':
                current_action[chat_id] = 'aссept_register_course'
            else:
                current_action[chat_id] = 'edit_workplan'
            workplan_data[chat_id].date = date
            work_with_plan.to_accept_workplan(call.message, workplan_data[chat_id])
        elif current_action[chat_id] == 'add_homework' or current_action[chat_id] == 'edit_homework':
            if current_action[chat_id] == 'add_homework':
                current_action[chat_id] = 'accept_add_homework'
            else:
                current_action[chat_id] = 'edit_homework'

            homework_data[chat_id].deadline = date
            work_with_homework.to_accept_homework(call.message, homework_data[chat_id])
    else:
        pass


@bot.callback_query_handler(func=lambda call: call.data == 'next-month')
def next_month(call):
    chat_id = call.message.chat.id
    saved_date = current_shown_dates.get(chat_id)
    if saved_date is not None:
        year, month = saved_date
        month += 1
        if month > 12:
            month = 1
            year += 1
        date = (year, month)
        current_shown_dates[chat_id] = date
        markup = create_calendar(year, month)
        bot.edit_message_text("Выберите дату", call.from_user.id, call.message.message_id, reply_markup=markup)
        bot.answer_callback_query(call.id, text="")
    else:
        pass


@bot.callback_query_handler(func=lambda call: call.data == 'previous-month')
def previous_month(call):
    chat_id = call.message.chat.id
    saved_date = current_shown_dates.get(chat_id)
    if saved_date is not None:
        year, month = saved_date
        month -= 1
        if month < 1:
            month = 12
            year -= 1
        date = (year, month)
        current_shown_dates[chat_id] = date
        markup = create_calendar(year, month)
        bot.edit_message_text("Выберите дату", call.from_user.id, call.message.message_id, reply_markup=markup)
        bot.answer_callback_query(call.id, text="")
    else:
        # Do something to inform of the error
        pass


@bot.callback_query_handler(func=lambda call: call.data == 'ignore')
def ignore(call):
    bot.answer_callback_query(call.id, text="")
