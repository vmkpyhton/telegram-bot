import datetime
import re
from telebot import types

from config import bot
from src.common_data import current_action, course_data, current_shown_dates, ExceptionMessage, message_to_user
from src.queries import get_teacher_courses, get_user_by_id
from src.models import Course, Homework
from src.calendar.telegramcalendar import create_calendar


def get_calendar(message):
    now = datetime.datetime.now()
    chat_id = message.chat.id
    date = (now.year, now.month)
    current_shown_dates[chat_id] = date
    markup = create_calendar(now.year, now.month)
    bot.send_message(message.chat.id, "Выберите дату", reply_markup=markup)


@bot.message_handler(regexp="course([0-9]{1,})")
def work_with_course(message):
    try:
        id_course = re.search("course([0-9]*)", message.text, re.IGNORECASE)
        if id_course:
            id_course = id_course.group(1)
            course_data[message.chat.id] = Course('', '')
            course_data[message.chat.id].get_course_info(id_course)

        if current_action[message.chat.id] == 'course_management' or current_action[message.chat.id] == 'edit_course':
            current_action[message.chat.id] = 'edit_course'
            markup = types.InlineKeyboardMarkup(row_width=1)
            markup.add(types.InlineKeyboardButton(
                text='Вернутсья в начало', callback_data='start'
            ), types.InlineKeyboardButton(
                text='Работать с планом курса', callback_data='work_with_plan'
            ), types.InlineKeyboardButton(
                text='Скачать статистику по курсу', callback_data='download_course_statistics'
            ), types.InlineKeyboardButton(
                text='Показать учеников', callback_data='show_students_by_course-' + id_course
            ), types.InlineKeyboardButton(
                text='Удалить курс', callback_data='delete_course-' + id_course
            ))
            bot.send_message(chat_id=message.chat.id,
                             text='\n'.join(
                                 [
                                     str(course_data[message.chat.id]),
                                     "Что вы хотите сделать с курсом",
                                     '/change_name_course - Изменить имя курса',
                                     '/change_short_info_course - Изменить краткую информацию курса',
                                     '/change_info_course - Изменить полную информацию курса',
                                     '/change_course_period - Изменить дату проведения курса'
                                 ]
                             ), reply_markup=markup)
        elif current_action[message.chat.id] == 'my_course':
            markup = types.InlineKeyboardMarkup(row_width=1)
            markup.add(types.InlineKeyboardButton(
                text='Вернутсья в начало', callback_data='start'
            ), types.InlineKeyboardButton(
                text='Показать все ДЗ', callback_data='show_all_homework_by_course-' + id_course
            ), types.InlineKeyboardButton(
                text='Показать доступные для сдачи ДЗ', callback_data='show_available_homework_by_course-' + id_course
            ), types.InlineKeyboardButton(
                text='Покинуть курс', callback_data='leave_course-' + id_course
            ))
            bot.send_message(chat_id=message.chat.id,
                             text='\n'.join(
                                 [
                                     str(course_data[message.chat.id]),
                                     "Что вы хотите сделать с курсом",
                                 ]), reply_markup=markup)

    except Exception as e:
        print(e)
        ExceptionMessage.show_started(message)


@bot.callback_query_handler(
    lambda call: call.data[:len('show_all_homework_by_course-')] == 'show_all_homework_by_course-' or call.data[:len(
        'show_available_homework_by_course-')] == 'show_available_homework_by_course-')
def show_all_homework_by_course(call):
    flag = 1
    if call.data[:len('show_available_homework_by_course-')] == 'show_available_homework_by_course-':
        flag = 0
        id_course = call.data[len('show_available_homework_by_course-'):]
    else:
        id_course = call.data[len('show_all_homework_by_course-'):]
    homeworks = Homework.get_all_homework_by_course(id_course)
    if flag == 0:
        homeworks = filter(lambda x: x.deadline >= datetime.date.today(), homeworks)
    bot.send_message(chat_id=call.message.chat.id,
                     text='\n'.join(
                         [
                             "Список ДЗ"
                         ] + ['/home{0} '.format(homework.id) + str(homework)
                              for homework in homeworks]
                     ))


@bot.callback_query_handler(lambda call: call.data == 'course_management')
def course_management(callback):
    try:
        message = callback.message
    except Exception as e:
        message = callback
    try:
        courses = ['/course{0} - {1}'.format(course['id_kurs'], course['name_kurs'])
                   for course in get_teacher_courses(message.chat.id)]
        current_action[message.chat.id] = 'course_management'
        markup = types.InlineKeyboardMarkup(row_width=2)
        markup.add(types.InlineKeyboardButton(
            text='Вернутсья в начало', callback_data='start'
        ), types.InlineKeyboardButton(
            text='Создать новый курс', callback_data='register_new_course'
        ))
        bot.send_message(chat_id=message.chat.id,
                         text='\n'.join(
                             [
                                 "Выберите курс c которым хотите работать",
                             ] + courses
                         ), reply_markup=markup)
    except Exception as e:
        print(e)
        ExceptionMessage.show_started(message)


@bot.callback_query_handler(lambda call: call.data == 'register_new_course')
def register_course(callback):
    try:
        message = callback.message
    except Exception as e:
        message = callback
    if current_action[message.chat.id] != 'course_management':
        ExceptionMessage.show_started(message)
    msg = bot.reply_to(message, """
    Начнем созданеи курса ^_^\nНапишите название курса
    """)
    current_action[message.chat.id] = 'register_new_course'
    bot.register_next_step_handler(msg, step_name_course)


@bot.message_handler(commands=['change_name_course'])
def rewrite_name_course(message):
    if current_action[message.chat.id] != 'edit_course':
        ExceptionMessage.show_started(message)
    msg = bot.reply_to(message, """
        Напишите название курса
        """)
    bot.register_next_step_handler(msg, step_name_course)


@bot.message_handler(commands=['change_short_info_course'])
def rewrite_short_info_course(message):
    if current_action[message.chat.id] != 'edit_course':
        ExceptionMessage.show_started(message)
    msg = bot.reply_to(message, 'Напишите краткую информцию курса')
    bot.register_next_step_handler(msg, step_short_info_course)


@bot.message_handler(commands=['change_info_course'])
def rewrite_info_course(message):
    if current_action[message.chat.id] != 'edit_course':
        ExceptionMessage.show_started(message)
    msg = bot.reply_to(message, 'Напишите полную информцию курса')
    bot.register_next_step_handler(msg, step_full_info)


@bot.message_handler(commands=['change_course_period'])
def rewrite_name_course(message):
    if current_action[message.chat.id] != 'edit_course':
        ExceptionMessage.show_started(message)
    bot.reply_to(message, 'Введите дату начала курса')
    get_calendar(message)


def step_name_course(message):
    try:
        chat_id = message.chat.id
        name = message.text
        if current_action[message.chat.id] != 'edit_course':
            course = Course(name, message.from_user.id)
            course_data[chat_id] = course
            msg = bot.reply_to(message, 'Напишите краткую информцию курса')
            bot.register_next_step_handler(msg, step_short_info_course)
        else:
            course_data[chat_id].name_course = name
            to_accept(message, course_data[chat_id])

    except Exception as e:
        msg = bot.reply_to(message, 'Ошибочка\n Напишите название курса')
        bot.register_next_step_handler(msg, step_name_course)


def step_short_info_course(message):
    try:
        chat_id = message.chat.id
        course_data[chat_id].short_info = message.text
        if current_action[message.chat.id] != 'edit_course':
            msg = bot.reply_to(message, 'Напишите полную информцию курса')
            bot.register_next_step_handler(msg, step_full_info)
        else:
            to_accept(message, course_data[chat_id])
    except Exception as e:
        msg = bot.reply_to(message, 'Ошибочка\n Напишите краткую информцию курса')
        bot.register_next_step_handler(msg, step_short_info_course)


def step_full_info(message):
    try:
        chat_id = message.chat.id
        course_data[chat_id].info = message.text
        if current_action[message.chat.id] != 'edit_course':
            current_action[message.chat.id] = 'date_start'
            bot.reply_to(message, 'Введите дату начала курса')
            get_calendar(message)
        else:
            to_accept(message, course_data[chat_id])
    except Exception as e:
        msg = bot.reply_to(message, 'Ошибочка\n Напишите полную информцию курса')
        bot.register_next_step_handler(msg, step_full_info)


def to_accept(message, сourse):
    markup = types.ReplyKeyboardMarkup()
    markup.add('Да', 'Нет')
    msg = bot.reply_to(message,
                       str(сourse) + '\nВсе верно?',
                       reply_markup=markup)
    bot.register_next_step_handler(msg, accept_register_course)


def accept_register_course(message):
    chat_id = message.chat.id
    if current_action[message.chat.id] != 'edit_course':
        if message.text == 'Да':
            course_data[chat_id].add_course()
            course_management(message)
        else:
            current_action[message.chat.id] = 'course_management'
            register_course(message)
    else:
        if message.text == 'Да':
            course_data[chat_id].update_course()
        current_action[message.chat.id] = 'course_management'
        work_with_course(message)


@bot.callback_query_handler(lambda call: call.data[:len('show_students_by_course-')] == 'show_students_by_course-')
def show_students_by_course(call):
    course_id = call.data[len('show_students_by_course-'):]
    students = Course.get_students(course_id)
    bot.send_message(call.message.chat.id,
                     text="Список студентов")
    for student in students:
        markup = types.InlineKeyboardMarkup(row_width=1)
        markup.add(
            types.InlineKeyboardButton('Написать письмо студенту',
                                       callback_data='send_message_to-' + str(student['id_student'])),
            types.InlineKeyboardButton('Показать ДЗ студента по курсу',
                                       callback_data=f"show_hw_course-student-{student['id_student']}-{course_id}"),
            types.InlineKeyboardButton(
                'Удалить студента с курса',
                callback_data=f"delete_student-student-{student['id_student']}-{course_id}"),
        )
        bot.send_message(call.message.chat.id,
                         text=f"Студент - {student['username']}\nТекущая средняя оценка - {student['avg_mark']}:\n",
                         reply_markup=markup)


@bot.callback_query_handler(lambda call: re.match(r'delete_student-student-([0-9]+)-([0-9]+)', call.data))
def delete_student_from_course(call):
    try:
        matched = re.match(r'delete_student-student-([0-9]+)-([0-9]+)', call.data)
        student, course = matched.group(1), matched.group(2)
        Course.leave_course(course, student)
        show_students_by_course(call)
    except Exception as e:
        print(e)
        show_students_by_course(call)


@bot.callback_query_handler(lambda call: re.match(r'show_hw_course-student-([0-9]+)-([0-9]+)', call.data))
def show_hw_course_by_student(call):
    try:
        matched = re.match(r'show_hw_course-student-([0-9]+)-([0-9]+)', call.data)
        student, course = matched.group(1), matched.group(2)
        student_username = get_user_by_id(student)
        course_homeworks = Homework.get_all_homework_by_course_for_student(course, student)
        bot.send_message(call.message.chat.id,
                         text=f'Информация по студенту {student_username[0][1]}\nИнфа по ДЗ - оценка\n' + '\n'.join(
                             ['{0} - {1}'.format(homework.information, homework.mark) for homework in
                              course_homeworks]))
        show_students_by_course(call)
    except Exception as e:
        print(Exception,e)
        show_students_by_course(call)


@bot.callback_query_handler(lambda call: call.data[:len('send_message_to-')] == 'send_message_to-')
def send_message_to_user(call):
    chat_id = call.message.chat.id
    msg = bot.send_message(chat_id, "Введите сообщение")
    message_to_user[chat_id] = call.data[len('send_message_to-'):]
    bot.register_next_step_handler(msg, send_message)


def send_message(message):
    chat_id = message.chat.id
    user_id = message_to_user[message.chat.id]
    markup = types.InlineKeyboardMarkup(row_width=1)
    markup.add(types.InlineKeyboardButton('Ответить', callback_data='send_message_to-' + str(chat_id)))
    bot.send_message(user_id, "У вас новое сообщение\n" + message.text, reply_markup=markup)


@bot.callback_query_handler(lambda call: call.data[:14] == 'delete_course-')
def delete_course(call):
    course_id = call.data[14:]
    Course.delete_course(course_id)
    course_management(call.message)
